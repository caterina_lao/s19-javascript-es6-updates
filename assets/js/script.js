/*
	ES6 Updates
		- standard for scriptong languages like JavaScript.
		-current version implemented into most browsers

	1. let and const
	2. Destructuring
		-allows us to break apart key structure into variable
		-Array Destructring	
*/

let employee = ['Sotto', 'Tito', 'Senate President', 'Male'];

let [lastName, firstName, position, gender] = employee; //one-to-one mapping

console.log(lastName);
console.log(position);

let player2 = ['Curry', 'Liliard', 'Paul', 'Irving'];
const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = player2;

console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

/*
	Object destructuring - allows us to destructure an object by allowing to add the values of an object's property into respective variables
*/

let pet = {
	species: 'dog',
	color: 'brown',
	breed: 'German Shepherd'
};

// let {breed, color} = pet;
// alert(`I have a ${breed} dog and it's color is ${color} `);

getPet({
	species: 'dog',
	color: 'black',
	breed: 'Dobermann'
})

// function getPet(options) {
// 	alert(`I have a ${options.breed} dog and it's color is ${options.color}`)
// };

// function getPet(options){
// 	let breed = options.breed;
// 	let color = options.color;
// 	console.log(`I have a ${breed} dog and it's color is ${color}`);
// };



// function getPet(options) {
// 	let {breed, color} = options;
// 	console.log(`I have a ${breed} dog and it's color is ${color}`);
// }

function getPet({breed, color}) {
	console.log(`I have a ${breed} dog and it's color is ${color}`);
};

//without function

let getPet2 = {
	species: 'dog',
	color: 'grey',
	breed: 'Siberian Husky'
};

let { breed, color} = getPet2;
console.log(`I have a ${breed} dog and it's color is ${color}.`);

/*
	let person = {
		name: <>,
		birthdate: <birthdate>;
		age:<>;
}

	**Create 3 new sentence variable which will contain the following strings:
		sentence1: Hi, I'm (name),
		sentence2: I was born on (bdate),
		sentence3: I am (age) years old

	Display the three sentences in console or in alert
	
*/
person({
		name: 'Catherine',
		birthdate: 'May 12, 1988',
		age:33
});

function person({name, birthdate, age}) {
	console.log(`Hi, I'm ${name}.`);
	console.log(`I was born on ${birthdate}.`);
	console.log(`I am ${age} years old.`);
};

let personA = {
		name: 'Paul Phoenix',
		birthday: 'August 5, 1995',
		age: 26
	};
let sentence1 = `Hi, I'm ${personA.name}`;
let sentence2 =`I was born on ${personA.birthday}`;
let sentence3 =`I am ${personA.age} years old.`;

console.log(sentence1);
console.log(sentence2);
console.log(sentence3);

const {age, name, birthday} = personA;

console.log(age);
console.log(name);
console.log(birthday);


let pokemon1 = {
	name1: 'Charmander',
	level: 15,
	type: 'fire',
	moves: ['ember', 'scratch', 'leer']
};


const {name1, level, type, moves} = pokemon1;
let sentence4 = `My pokemon is ${name1}. It is level ${level}. It is a ${type} and it movements are ${moves}.`

console.log(moves);

const [move1,,move3] = moves;
console.log(move1);
console.log(move3);

pokemon1.name1 = 'Meowth';
console.log(name1);
console.log(pokemon1);

// Arrow Functions - fat arrow

//  before ES6 
function printFullName (firstName, middleInitial, lastName) {
	return firstName + ' ' + middleInitial + ' ' + lastName;
};

//in ES6

const printFullName1 = (firstName, middleInitial, lastName) =>{
	return firstName + ' ' + middleInitial + ' ' + lastName;
};

// traditional function

function displayMsg(){
	console.log(`Hello World!`);
};

const hello = () => {
	console.log(`Hello from arrow`);
};

hello();

// 

function greet(pokemon1){
	console.log(`Hi, ${pokemon1.name1}! ")`);
};

greet(pokemon1);

// 

const greet1 = (pokemon1) => {
	console.log(`Hi, ${pokemon1.name1}! ")`);
};
greet1(pokemon1);


// Implicit Return - allows us to return a value without the use of return keyword

function addNum(num1, num2) {
	console.log(num1 + num2);
};
addNum(3, 5);

const addNum1 = (num1, num2) => num1 + num2;
let sum = addNum1(5, 6);
console.log(sum);

//this keyword

let protagonist = {
	name: 'Caterina',
	occupation: 'soldier',
	greet: function(){
		// tradional methods would have this keyword refer to the parent object
		console.log(this);
		console.log(`Hi! I'm ${this.name}.`)
	},
	introduceJob: function() { //if ()=> output will be undefined
		console.log(this);
		console.log(`I work as ${this.occupation}.`);
	}
};

protagonist.greet();
protagonist.introduceJob();

//Class-base Object Blueprints
	//Classes are templates of objects

// Create class 

function Pokemon(name, type, level) {
	this.name = name;
	this.type = type;
	this.level = level;
}


// ES6 Class Creation

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

let car1 = new Car('Toyota', 'Vios', '2002');
console.log(car1);
let car2 = new Car('Cooper', 'Mini', '1969');
console.log(car2);
let car3 = new Car('Porsche', '911', '1960');
console.log(car3);



console.log('Activity:');

// 1. Update and Debug the following codes to ES6
// 		Use template literals
// 		Use array/object destructuring
// 		Use arrow function
// 2. Create a class constructor able to receive 3 arguments
// 		- it should be able to receive two strings and a number
// 		- Using the this keyword assign properties:
// 			name,
// 			breed,
// 			dogAge = <7 * human years>
// 				- assign the parameters as values to each property.
// 3. Create 2 new objects using our class constructor
// 	This constructor shoule be able to create Dog objects.
// 	Log the 2 new Dog objects in the console or alert.



let student1 = {
	name1A: 'Shawn Michaels',
	birthday1A: 'May 5, 2003',
	age1A: 18,
	isEnrolled1A: true,
	classes1: ['Philosphy 101', 'Social Sciences 201']
}

const {name1A, birthday1A, age1A, classes1} = student1;
let sentence5 = `Hi! I'm  ${name1A}. I am ${age1A} years old.` 
let sentence6 = `I study the following courses ${classes1}.`


console.log(classes1);

const [classes1A, classes1B] = classes1;
console.log(classes1A);
console.log(classes1B);

console.log(sentence5);
console.log(sentence6);


let student2 = {
	name2: 'Steve Austin',
	birthday2: 'June 15, 2001',
	age2: 20,
	isEnrolled2: true,
	classes2: ['Philosphy 401', 'Natural Sciences 402'],
}


const introduce = (student2) =>{

	//Note: You can destructure objects inside functions.
	
	console.log(`Hi! I'm ${student2.name2}. I am ${student2.age2} years old.`);
	console.log(`I study the following courses ${student2.classes2}.`);
}

introduce(student2);


const getCube = (num) =>{
	console.log(Math.pow(num, 3));
}

getCube(3);



let numArr1 = [15, 16, 32, 21, 21, 2];
numArr1.forEach(function(numArr1){
	console.log(numArr1);
});


let numsSquared = numArr1.map(function(numArr1){
	console.log(numArr1);
	return numArr1 ** 2;
});
console.log(numsSquared);

class dogA {
	constructor(name, breed, dogAge1){
		this.name = name;
		this.breed = breed;
		this.dogAge1 = dogAge1 * 7;
	}
};

let dogA1 = new dogA('Blackie', 'Husky', 1);
console.log(dogA1);
let dogA2 = new dogA('Pepper', 'Poodle', 1);
console.log(dogA2);
// let dogA3 = new dogA('Onion', 'Labrador', 5);
// console.log(dogA3);

